from typing import Optional, List

from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel

app = FastAPI()


class Lesson(BaseModel):
    id: str
    startTime: int
    endTime: int
    lessonName: str
    lessonLinks: List[str]


class Message(BaseModel):
    message: str


lessons: [Lesson] = []


@app.get("/lessons", response_model=List[Lesson])
def get_lessons():
    return lessons


@app.get("/lessons/{lesson_id}", response_model=Lesson, responses={404: {"model": Message}})
def get_lesson_by_id(lesson_id: str):
    print(lessons)
    for les in lessons:
        if les.id == lesson_id:
            return les
    return JSONResponse(status_code=404, content={"message": "Lesson not found"})


@app.post("/lessons", responses={400: {"model": Message}})
def add_lesson(lesson: Lesson):
    for les in lessons:
        if les.id == lesson.id:
            return JSONResponse(status_code=400, content={"message": "Already have lesson with such ID"})
    lessons.append(lesson)
    return "success"


@app.put("/lessons", responses={404: {"model": Message}})
def update_lesson(lesson: Lesson):
    for i in range(len(lessons)):
        if lessons[i].id == lesson.id:
            lessons[i] = lesson
            return "success"
    return JSONResponse(status_code=404, content={"message": "Lesson not found"})


@app.delete("/lessons/{lesson_id}", responses={404: {"model": Message}})
def delete_lesson_by_id(lesson_id: str):
    for i in range(len(lessons)):
        if lessons[i].id == lesson_id:
            lessons.pop(i)
            return "success"
    raise HTTPException(status_code=404, detail="Lesson not found")
