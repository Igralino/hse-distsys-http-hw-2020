import os

import requests
import json

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

lesson = {"id": "kekus",
          "startTime": 1123312,
          "endTime": 1125312,
          "lessonName": "First Lesson Name",
          "lessonLinks": [
              "https://vk.com/iii"
          ]}


def test_empty_array():
    response = requests.get(URL + '/lessons')
    assert response.text == json.dumps([])
    assert response.status_code == 200


def test_lesson_404():
    response = requests.get(URL + '/lessons/123123123')
    assert response.status_code == 404


def test_lesson_post_and_get():
    data = json.dumps(lesson)
    response = requests.post(URL + '/lessons', data=data)
    assert response.status_code == 200

    response = requests.get(URL + '/lessons/kekus')
    assert response.status_code == 200
    assert response.json() == lesson


def test_not_empty_array():
    response = requests.get(URL + '/lessons')
    assert response.json() == [lesson]
    assert response.status_code == 200


def test_same_id():
    data = json.dumps(lesson)
    response = requests.post(URL + '/lessons', data=data)
    assert response.status_code == 400


def test_update():
    new_name = "English lesson name"
    lesson["lessonName"] = new_name
    data = json.dumps(lesson)
    response = requests.put(URL + '/lessons', data=data)
    assert response.status_code == 200

    response = requests.get(URL + '/lessons/kekus')
    response.json()["lessonName"] = new_name
    assert response.status_code == 200


def test_delete():
    response = requests.delete(URL + '/lessons/kekus')
    assert response.status_code == 200

    response = requests.get(URL + '/lessons')
    assert response.text == json.dumps([])
    assert response.status_code == 200
